#ifndef PRODUCER_H
#define PRODUCER_H

#include <QDebug>
#include <QThread>
#include <QMutex>
#include <QReadWriteLock>
#include <QWaitCondition>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "resources.h"

class Producer : public QThread
{
    Q_OBJECT
public:
    explicit Producer(Resources *r, QObject *parent = 0);
    ~Producer();

    void stopProduce();
    
protected:
    Resources *r;
    int frameCount;

    void run();

private:
    QMutex abortMutex;
    bool abort;

    QMutex videoMutex;
    cv::VideoCapture video;
    int emptyFrameCounter;

    QMutex fpsMutex;
    int fps;

signals:
    void newFrame(cv::Mat);
    void finished();

public slots:
    //bool endCapture();

};

#endif // PRODUCER_H
