#-------------------------------------------------
#
# Project created by QtCreator 2013-08-03T20:13:28
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProdConsCV
TEMPLATE = app

macx {
    QMAKE_CFLAGS_X86_64 += -mmacosx-version-min=10.7
    QMAKE_CXXFLAGS_X86_64 = $$QMAKE_CFLAGS_X86_64

        message("* Using settings for Mac OS X.")
    INCLUDEPATH += /usr/local/include/opencv

    LIBS += -L/usr/local/lib/ \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc -framework GLUT -framework OpenGL -framework Cocoa
}

SOURCES += main.cpp \
    producer.cpp \
    consumer.cpp \
    opencv_glwidget.cpp

HEADERS  += \
    producer.h \
    consumer.h \
    resources.h \
    mainwidget.h \
    opencv_glwidget.h

FORMS    +=
