#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QVBoxLayout>
#include "resources.h"
#include "producer.h"
#include "consumer.h"
#include "opencv_glwidget.h"

class MainWidget : public QWidget
{
    Q_OBJECT
    
public:
    //------------------------------------------------------------------ ctor dtor
    explicit MainWidget(QWidget *parent = 0) : QWidget(parent) {

        qRegisterMetaType<cv::Mat>("cv::Mat");

        initGUI();

        startAll();
    }

    ~MainWidget()
    {
        stopAll();
        sleep(1);
    }

    //------------------------------------------------------------------ 1 prod, n cons
    void startAll()
    {
        initConsumers();
        sleep(1);
        initProducer();
    }

    void initProducer()
    {
        producer = new Producer(&r);
        connect(producer, SIGNAL(newFrame(cv::Mat)),
                this, SLOT(showProducer(cv::Mat)));
        connect(producer, SIGNAL(finished()),
                producer, SLOT(deleteLater()));

        producer->start();
    }

    void initConsumers()
    {
        consumerCounter = 0;

        consumer1 = new Consumer(1, &r);
        connect(consumer1, SIGNAL(newComputation(cv::Mat)),
                this, SLOT(showConsumer1(cv::Mat)));
        connect(consumer1, SIGNAL(finished()),
                consumer1, SLOT(deleteLater()));
        connect(consumer1, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer2 = new Consumer(2, &r);
        connect(consumer2, SIGNAL(newComputation(cv::Mat)),
                this, SLOT(showConsumer2(cv::Mat)));
        connect(consumer2, SIGNAL(finished()),
                consumer2, SLOT(deleteLater()));
        connect(consumer2, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer3 = new Consumer(3, &r);
        connect(consumer3, SIGNAL(newComputation(cv::Mat)),
                this, SLOT(showConsumer3(cv::Mat)));
        connect(consumer3, SIGNAL(finished()),
                consumer3, SLOT(deleteLater()));
        connect(consumer3, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer1->start();
        consumer2->start();
        consumer3->start();
    }

    void stopAll()
    {
        stopProducer();
        stopConsumers();
        qDebug() << "wake all!";
        r.waitCondition.wakeAll();
    }

    void stopProducer() {
        producer->stopProduce();
    }

    void stopConsumers() {
        consumer1->stopConsume();
        consumer2->stopConsume();
        consumer3->stopConsume();
    }

    //------------------------------------------------------------------ gui
    void initGUI() {
        QVBoxLayout *l = new QVBoxLayout;
        this->producerLabel = new OpenCV_GLWidget;
        l->addWidget(producerLabel);
        this->consumer1Label = new OpenCV_GLWidget;
        l->addWidget(consumer1Label);
        this->consumer2Label = new OpenCV_GLWidget;
        l->addWidget(consumer2Label);
        this->consumer3Label = new OpenCV_GLWidget;
        l->addWidget(consumer3Label);
        this->setLayout(l);
    }

    void keyPressEvent(QKeyEvent* event) {
        if(event->key() == Qt::Key_S) {
            qDebug() << "pressed s, stop all";
            stopAll();
        }
    }

    void closeEvent(QCloseEvent * event)
    {
        emit shutdown();
        this->close();
    }

private:
    OpenCV_GLWidget *producerLabel;
    OpenCV_GLWidget *consumer1Label;
    OpenCV_GLWidget *consumer2Label;
    OpenCV_GLWidget *consumer3Label;

    Resources r;
    Producer *producer;
    Consumer *consumer1;
    Consumer *consumer2;
    Consumer *consumer3;

    int consumerCounter;

private slots:
    void showProducer(cv::Mat cvFrame) {
        //this->producerLabel->setText(QString("Producer: %1").arg(n));
        QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
        qtFrame = qtFrame.rgbSwapped();
        this->producerLabel->renderImage(qtFrame);
    }
    void showConsumer1(cv::Mat cvFrame) {
        //this->consumer1Label->setText(QString("Consumer1: %1").arg(n));
        QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
        qtFrame = qtFrame.rgbSwapped();
        this->consumer1Label->renderImage(qtFrame);
    }
    void showConsumer2(cv::Mat cvFrame) {
        //this->consumer2Label->setText(QString("Consumer2: %1").arg(n));
        QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
        qtFrame = qtFrame.rgbSwapped();
        this->consumer2Label->renderImage(qtFrame);
    }
    void showConsumer3(cv::Mat cvFrame) {
        //this->consumer3Label->setText(QString("Consumer3: %1").arg(n));
        QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
        qtFrame = qtFrame.rgbSwapped();
        this->consumer3Label->renderImage(qtFrame);
    }

    void incrementConsumerCounter() {
        consumerCounter++;
        if(consumerCounter==3) {
            qDebug() << "..... could restart!";
            startAll();
        }
    }

signals:
    void shutdown();
};

#endif // MAINWIDGET_H
