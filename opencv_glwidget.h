#ifndef OPENCV_GLWIDGET_H_
#define OPENCV_GLWIDGET_H_

#include <QGLWidget>
#include <QImage>

class OpenCV_GLWidget: public QGLWidget
{
    Q_OBJECT
public:
    OpenCV_GLWidget(QWidget * parent = 0, const QGLWidget * shareWidget = 0, Qt::WindowFlags f = 0);
    virtual ~OpenCV_GLWidget();

public slots:
    void renderImage(const QImage& frame);

protected:
    virtual void paintGL();
    virtual void resizeGL(int width, int height);

private:
    QImage m_GLFrame;
};

#endif /* OPENCV_GLWIDGET_H_ */
