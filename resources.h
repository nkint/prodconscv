#ifndef RESOURCES_H
#define RESOURCES_H

#define THREAD_VERBOSE 1

#include <QDebug>
#include <QReadWriteLock>
#include <QWaitCondition>
#include <opencv2/core/core.hpp>

class Resources {

public:

    Resources() {
        qDebug() << "Resources > ctor";
        lastFrameCount = -1;
    }

    ~Resources() {
        qDebug() << "Resources > dtor";
    }

    int lastFrameCount;
    cv::Mat lastFrame;
    QReadWriteLock lastFrame_read;
    QWaitCondition waitCondition;

};

#endif // RESOURCES_H
