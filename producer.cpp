#include "producer.h"

#define ABORT_POLICY_LIMIT 7

Producer::Producer(Resources *r, QObject *parent) :
    QThread(parent),
    abort(false),
    r(r),
    frameCount(0),
    emptyFrameCounter(0),
    fps(20)
{
    qDebug() << "producer > ctor";

    videoMutex.lock();
    video = cv::VideoCapture(0);
    videoMutex.unlock();
}

Producer::~Producer() {
    qDebug() << "Producer dtor";
    stopProduce();

    videoMutex.lock();
    if(video.isOpened()) {
        video.release();
    }
    videoMutex.unlock();

    wait();
}

void Producer::stopProduce() {
    abortMutex.lock();
    abort = true;
    abortMutex.unlock();
}

void Producer::run()
{
    forever {
        //-------------------------------- check for abort
        abortMutex.lock();
        if(abort) {
            abortMutex.unlock();
            break;
        } abortMutex.unlock();
        //-------------------------------- check for abort

        //-------------------------------- produce
        frameCount++;

        //if(THREAD_VERBOSE) qDebug() << "Producer > producing: " << frameCount;

        cv::Mat cvFrame;
        video >> cvFrame;
        cv::pyrDown(cvFrame, cvFrame);

        if(cvFrame.empty())  {
            emptyFrameCounter++;
            if( emptyFrameCounter>ABORT_POLICY_LIMIT ) break;
            continue;
        }
        //-------------------------------- produce

        //-------------------------------- write in resources
        r->lastFrame_read.lockForWrite();

        r->lastFrame = cvFrame;
        r->lastFrameCount = frameCount;
        emit newFrame(cvFrame);

        r->waitCondition.wakeAll();
        r->lastFrame_read.unlock();
        //-------------------------------- write in resources

        fpsMutex.lock();
        msleep(fps);
        fpsMutex.unlock();
    }
    emit finished();
}
