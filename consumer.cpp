#include "consumer.h"

Consumer::Consumer(int id, Resources *r, QObject *parent):
    QThread(parent),
    abort(false)
{
    this->r = r;
    this->id = id;
}

Consumer::~Consumer() {
    qDebug() << QString("Consumer %1 dtor").arg(id);
    stopConsume();
    wait();
}

void Consumer::stopConsume() {
    abortMutex.lock();
    abort = true;
    abortMutex.unlock();
}

void Consumer::consumeMessage(cv::Mat cvFrame, int frameCount)
{
    if(THREAD_VERBOSE) qDebug() << QString("#%1 consuming: %2").arg(id).arg(frameCount);

    msleep(1000*id);
    emit newComputation(cvFrame);
}

void Consumer::run()
{
    forever {
        //-------------------------------- acquire the new frame
        r->lastFrame_read.lockForRead();
        // if there isn't new frame..
        if(r->lastFrameCount==-1 && r->lastFrameCount <= lastFrameCount) {
            if(THREAD_VERBOSE) qDebug() << QString("Consumer %1: going to sleep").arg(id);
            r->waitCondition.wait(&r->lastFrame_read);
            if(THREAD_VERBOSE) qDebug() << QString("Consumer %1: awake").arg(id);
        }
        lastFrameCount = r->lastFrameCount;
        lastFrame = r->lastFrame;

        r->lastFrame_read.unlock();
        //-------------------------------- acquire the new frame

        //-------------------------------- do the computations
        consumeMessage(lastFrame, lastFrameCount);
        //-------------------------------- do the computations

        //-------------------------------- check for abort
        abortMutex.lock();
        if(abort) {
            abortMutex.unlock();
            if(THREAD_VERBOSE) qDebug() << QString("Consumer %1: abort..").arg(id);
            break;
        } abortMutex.unlock();
        //-------------------------------- check for abort
    }
    emit finished();
}
